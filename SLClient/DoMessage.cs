﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DarkFrameMaster
{
	public class DoMessage
	{
		public String DoClick(int x, int y)
		{
			JObject action = JObject.FromObject(new
			{
				action = "Click_Scanner",
				cx = x,
				cy = y
			});
			return action.ToString();
		}
	}
}
