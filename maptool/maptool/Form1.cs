﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Tools;


namespace maptool
{
    public partial class maptool : Form
    {
        public static Image[] mapRes = new Image[20];
        public int tempImage = 951357; // 设置一个永远不用的值
        public int[][] map = new int[10][];

        public static int MAPRES_WALL = 0;
        public static int MAPRES_ROAD = 1;
        public static int MAPRES_KEY = 2;
        public static int MAPRES_DOOR = 3;
        public static int MAPRES_IN = 4;
        public static int MAPRES_GOLD = 5;
        public static int MAPRES_NPC = 6;
        public static int MAPRES_ENEMY = 7;
        public static int MAPRES_OUT = 8;
        public static int MAPRES_LEAD = 9;
        public static int MAPRES_NOGOLD = 10;
        public static int MAPRES_DE = 11;
        public maptool()
        {
            mapRes[0] = System.Drawing.Image.FromFile("../img/wall.png");
            mapRes[1] = System.Drawing.Image.FromFile("../img/road.png");
            mapRes[2] = System.Drawing.Image.FromFile("../img/key.png");
            mapRes[3] = System.Drawing.Image.FromFile("../img/door.png");
            mapRes[4] = System.Drawing.Image.FromFile("../img/stairs.png");
            mapRes[5] = System.Drawing.Image.FromFile("../img/gold.png");
            mapRes[6] = System.Drawing.Image.FromFile("../img/npc.png");
            mapRes[7] = System.Drawing.Image.FromFile("../img/enemy.png");
            mapRes[8] = System.Drawing.Image.FromFile("../img/stairs.png");
            mapRes[9] = System.Drawing.Image.FromFile("../img/lead.jpg");
            mapRes[10] = System.Drawing.Image.FromFile("../img/noGold.png");
            mapRes[11] = System.Drawing.Image.FromFile("../img/de.png");
            InitializeComponent();
            initMapPanel();
            initPicPanel();
        }

        public void initMapPanel()
        {
            for (int i = 0;i < map.Length; i++)
            {
                map[i] = new int[10];
                for (int j = 0;j < map[i].Length;j ++)
                {
                    PixBox pix = new PixBox();
                    pix.x = i;
                    pix.y = j;

                    pix.Size = new Size(50,50);
                    pix.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; ;
                    pix.Image = mapRes[MAPRES_DE];
                    pix.Click += new System.EventHandler(this.SetMapPic);

                    map[pix.x][pix.y] = MAPRES_DE;
                    this.MapPanel.Controls.Add(pix);
                }
            }
        }

        public void initPicPanel()
        {
            for (int i = 0;i< mapRes.Length;i++)
            {
                if (mapRes[i] == null)
                {
                    break;
                }
                Image img = mapRes[i];
                PixBox tempP = new PixBox();
                tempP.Size = new Size(50,50);
                tempP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; 
                tempP.Image = img;
                tempP.x = i;

                tempP.Click += new System.EventHandler(this.pictureBox_Click);
                this.PicPanel.Controls.Add(tempP);
            }
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PixBox tempBox = (PixBox)sender;
            tempImage = tempBox.x;
        }

        private void SetMapPic(object sender, EventArgs e)
        {
            PixBox tempBox = (PixBox)sender;
            if (tempImage != 951357)
            {
                tempBox.Image = mapRes[tempImage];
                map[tempBox.x][tempBox.y] = tempImage;
                Console.WriteLine("......"+ tempImage);
            } 
        }


        private void button1_Click(object sender, EventArgs e)
        {
            MapData md = new MapData();
            String id = this.textBox1.Text;
            md.id = id;
            md.map = map;
            md.CreateMapData(md);
        }
    }

    // 包装类
    public class PixBox: PictureBox
    {
        public int x;
        public int y;
    }
}
