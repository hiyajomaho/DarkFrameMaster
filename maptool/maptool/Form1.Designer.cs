﻿namespace maptool
{
    partial class maptool
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.MapPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.PicPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.leval = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MapPanel
            // 
            this.MapPanel.Location = new System.Drawing.Point(12, 4);
            this.MapPanel.Name = "MapPanel";
            this.MapPanel.Size = new System.Drawing.Size(520, 550);
            this.MapPanel.TabIndex = 0;
            // 
            // PicPanel
            // 
            this.PicPanel.Location = new System.Drawing.Point(538, 4);
            this.PicPanel.Name = "PicPanel";
            this.PicPanel.Size = new System.Drawing.Size(208, 550);
            this.PicPanel.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(765, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(94, 21);
            this.textBox1.TabIndex = 2;
            // 
            // leval
            // 
            this.leval.AutoSize = true;
            this.leval.Location = new System.Drawing.Point(766, 17);
            this.leval.Name = "leval";
            this.leval.Size = new System.Drawing.Size(35, 12);
            this.leval.TabIndex = 3;
            this.leval.Text = "level";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(768, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 33);
            this.button1.TabIndex = 4;
            this.button1.Text = "生成";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // maptool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 577);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.leval);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.PicPanel);
            this.Controls.Add(this.MapPanel);
            this.Name = "maptool";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel MapPanel;
        private System.Windows.Forms.FlowLayoutPanel PicPanel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label leval;
        private System.Windows.Forms.Button button1;
    }
}

