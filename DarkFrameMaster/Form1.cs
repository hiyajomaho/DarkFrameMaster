﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkFrameMaster.Tools;

namespace DarkFrameMaster
{
	public partial class Form1 : Form
	{
		private static bool isDebug;
		private static Panel debugPanel;
		private static Label debugLabel;

		private static PictureBox[,] mapBD = new PictureBox[15,15];
		private static int[,] mapStatus = new int[15, 15];

		private static Stream temp;
		private static Stream temp2;

		public Form1(bool debugMode)
		{
			isDebug = debugMode;
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			this.Text = (isDebug ? "[DEBUG MODE] " : "") + "魔塔";
			if (isDebug)
			{
				this.Size = new Size(this.Size.Width + 200, this.Size.Height);
				this.mainPanel.Size = new Size(this.mainPanel.Size.Width + 200, this.mainPanel.Size.Height);
				debugPanel = new System.Windows.Forms.Panel();
				debugPanel.Location = new Point(600, 0);
				debugPanel.Size = new Size(200, 450);
				debugPanel.BackColor = Color.White;
				this.mainPanel.Controls.Add(debugPanel);

				debugLabel = new System.Windows.Forms.Label();
				debugLabel.Location = new Point(0, 0);
				debugLabel.Size = new Size(200, 450);
				debugLabel.BackColor = Color.White;
				debugLabel.ForeColor = Color.Black;
				debugLabel.BorderStyle = BorderStyle.Fixed3D;
				debugPanel.Controls.Add(debugLabel);

				doDebug("正在进入DEBUG模式");
				doDebug("进入DEBUG模式成功");
			}
			//
			try
			{
				temp = File.Open("./logo.png", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
				temp2 = File.Open("./logo2.png", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
				for (int i = 0; i < 15; i++)
				{
					for (int j = 0; j < 15; j++)
					{
						mapBD[i, j] = new System.Windows.Forms.PictureBox();
						if (mapBD[i, j] != null)
						{
							mapBD[i, j].Size = new Size(450 / 15, 450 / 15);
							mapBD[i, j].Location = new Point(i * (450 / 15), j * (450 / 15));
							mapBD[i, j].BackColor = Color.Empty;
							mapBD[i, j].BackgroundImage = Image.FromStream(temp);
							mapBD[i, j].BackgroundImageLayout = ImageLayout.Zoom;
							mapBD[i, j].BorderStyle = BorderStyle.Fixed3D;
							mapBD[i, j].Click += new EventHandler(testClick);
							this.gamePanel.Controls.Add(mapBD[i, j]);
							mapStatus[i, j] = 1;
							//doDebug("图片控件生成成功！x:" + i + " y:" + j);
						}
						else
						{
							doDebug("图片控件生成失败！");
						}
					}
				}
			}
			catch
			{
				doDebug("图片加载失败！");
			}
			//*/
		}

		private void testClick(object sender, EventArgs e)
		{
			PictureBox obj = sender as PictureBox;
			for(int i = 0; i < 15; i++)
			{
				for(int j = 0; j < 15; j++)
				{
					if (mapBD[i,j] == obj)
					{
						doDebug("点击->x:" + i + ", y:" + j);
						if (mapStatus[i, j] == 1)
						{
							obj.BackgroundImage = Image.FromStream(temp2);
							mapStatus[i, j] = 2;
						}
						else
						{
							obj.BackgroundImage = Image.FromStream(temp);
							mapStatus[i, j] = 1;
						}
					}
				}
			}
		}

		private void doDebug(string demsg, bool isShow = true)
		{
			string msg = demsg;
			string tmsg = "[" + DateTime.Now.ToString() + "]\n" + msg;
			if (isDebug && isShow)
			{
				debugLabel.Text = tmsg + "\n" +debugLabel.Text;
			}
			Console.WriteLine(tmsg);
			//Console.WriteLine("[Console Log]" + debugLabel.Text);
			return;
		}

		private void label1_Click(object sender, EventArgs e)
		{
			this.label1.Text = "don't do this";
		}
	}
}
