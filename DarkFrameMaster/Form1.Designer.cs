﻿namespace DarkFrameMaster
{
	partial class Form1
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.mainPanel = new System.Windows.Forms.Panel();
			this.gamePanel = new System.Windows.Forms.Panel();
			this.dataPanel = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.mainPanel.SuspendLayout();
			this.dataPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainPanel
			// 
			this.mainPanel.Controls.Add(this.gamePanel);
			this.mainPanel.Controls.Add(this.dataPanel);
			this.mainPanel.Location = new System.Drawing.Point(0, 0);
			this.mainPanel.Margin = new System.Windows.Forms.Padding(0);
			this.mainPanel.Name = "mainPanel";
			this.mainPanel.Size = new System.Drawing.Size(600, 450);
			this.mainPanel.TabIndex = 1;
			// 
			// gamePanel
			// 
			this.gamePanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.gamePanel.Location = new System.Drawing.Point(150, 0);
			this.gamePanel.Margin = new System.Windows.Forms.Padding(0);
			this.gamePanel.Name = "gamePanel";
			this.gamePanel.Size = new System.Drawing.Size(450, 450);
			this.gamePanel.TabIndex = 1;
			// 
			// dataPanel
			// 
			this.dataPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.dataPanel.Controls.Add(this.label1);
			this.dataPanel.Location = new System.Drawing.Point(0, 0);
			this.dataPanel.Margin = new System.Windows.Forms.Padding(0);
			this.dataPanel.Name = "dataPanel";
			this.dataPanel.Size = new System.Drawing.Size(150, 450);
			this.dataPanel.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoEllipsis = true;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(4, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "label1";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.ClientSize = new System.Drawing.Size(604, 451);
			this.Controls.Add(this.mainPanel);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.mainPanel.ResumeLayout(false);
			this.dataPanel.ResumeLayout(false);
			this.dataPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel mainPanel;
		private System.Windows.Forms.Panel dataPanel;
		private System.Windows.Forms.Panel gamePanel;
		private System.Windows.Forms.Label label1;
	}
}

