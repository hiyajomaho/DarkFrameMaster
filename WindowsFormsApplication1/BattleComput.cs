﻿using System;

namespace Game.Tools
{
	public class BattleComput
	{

		public static Random random = new Random();

		public void DMGTestTools()
		{
			double atk = random.Next(1, 2000000);
			double def = random.Next(1, 2000);

			long dmg = DMGComput(atk, def);
		}
		public long DMGComput(double atk, double def)
		{
			double ATK = atk > 0 ? atk : 1;
			double DEF = def;

			double dmg = 0;

			dmg = (ATK * (1 - ((DEF * 0.04) / (1 + DEF * 0.04)) * DEF / (ATK * 2)));		//类型1 较为成熟，但是会有负数的情况，需要进行特殊处理
			dmg = ATK * (1 - (DEF / (400 + DEF)));                                          //阴阳师采用的伤害计算公式（伤害稳定、无负数、但是会出现高攻高防伤害极度削减）
			dmg = ATK * ATK / (ATK + DEF);                                                  //较为成熟的一个类型，测试覆盖不全面，暂时不进行说明
			//dmg = ATK - DEF;																//回合制中最常出现，但是会有很大几率负数

			long finalDMG = (long)dmg;

			Console.WriteLine("攻击力:{0}  防御力:{1}  造成伤害:{2}  攻伤比:{3}  攻防比:{4}", atk, def, finalDMG, (finalDMG / atk), (atk/def));
			if (finalDMG < 0) return 0;
			return finalDMG;
		}
	}
}