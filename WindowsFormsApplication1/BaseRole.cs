﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using operationClass;

namespace WindowsFormsApplication1
{ 
    public class BaseRole  //所有角色的基类
    {
        int id;
        string name;
        int type;
        int camp;  //阵营

        int Aggressivity;  // 攻，防，血
        int Defenses;
        double LifeValue;

        PropClass arms;  // 武器
        PropClass Armor; // 防具

        string desc; // 描述

        public BaseRole()
        {

        }
        public BaseRole(string name,int type, int camp, int Aggressivity, int Defenses, int LifeValue)
        {
            this.Name = name;
            this.type = type;
            this.camp = camp;
            this.Aggressivity1 = Aggressivity;
            this.Defenses = Defenses;
            this.LifeValue = LifeValue;
        }
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public PropClass Arms
        {
            get
            {
                return arms;
            }

            set
            {
                arms = value;
            }
        }

        public PropClass Armor1
        {
            get
            {
                return Armor;
            }

            set
            {
                Armor = value;
            }
        }

        public double LifeValue1
        {
            get
            {
                return LifeValue;
            }

            set
            {
                LifeValue = value;
            }
        }

        public int Aggressivity1
        {
            get
            {
                return Aggressivity;
            }

            set
            {
                Aggressivity = value;
            }
        }
    }
    
}
