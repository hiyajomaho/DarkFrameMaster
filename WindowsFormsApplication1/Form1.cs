﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using operationClass;
using Game.Tools;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        RoomClass tempRoom = new RoomClass(); // 房间

        // 0：墙  1：路 2：钥匙 3：门 4：入口楼梯 5：宝箱 6 npc 7 敌人 8：出口楼梯 9 猪脚 10 宝箱(空)
        public static Image[] mapRes = new Image[20];

        public static int MAPRES_WALL = 0;
        public static int MAPRES_ROAD = 1;
        public static int MAPRES_KEY = 2;
        public static int MAPRES_DOOR = 3;
        public static int MAPRES_IN = 4;
        public static int MAPRES_GOLD = 5;
        public static int MAPRES_NPC = 6;
        public static int MAPRES_ENEMY = 7;
        public static int MAPRES_OUT = 8;
        public static int MAPRES_LEAD = 9;
        public static int MAPRES_NOGOLD = 10;

		public static BattleComput bc = new BattleComput();
		public Form1()
        {

            mapRes[0] = System.Drawing.Image.FromFile("../img/wall.png");
            mapRes[1] = System.Drawing.Image.FromFile("../img/road.png");
            mapRes[2] = System.Drawing.Image.FromFile("../img/key.png");
            mapRes[3] = System.Drawing.Image.FromFile("../img/door.png");
            mapRes[4] = System.Drawing.Image.FromFile("../img/stairs.png");
            mapRes[5] = System.Drawing.Image.FromFile("../img/gold.png");
            mapRes[6] = System.Drawing.Image.FromFile("../img/npc.png");
            mapRes[7] = System.Drawing.Image.FromFile("../img/enemy.png");
            mapRes[8] = System.Drawing.Image.FromFile("../img/stairs.png");
            mapRes[9] = System.Drawing.Image.FromFile("../img/lead.jpg");
            mapRes[10] = System.Drawing.Image.FromFile("../img/noGold.png");

            InitializeComponent();
            int id = reLoad_Map();
            reLoad_PeoPle(id);
        }

        public int reLoad_Map()
        {
			/*
            int[][] map2 = new int[][]
           {
               new int[]{0,1,1,1,8,8,1,1,1,1 },
               new int[]{0,1,0,1,1,1,1,0,0,1 },
               new int[]{0,1,0,0,0,3,0,0,0,1 },
               new int[]{0,1,0,0,2,7,0,5,0,1 },
               new int[]{0,1,0,0,0,0,0,3,0,1 },
               new int[]{0,1,0,0,0,6,1,1,1,1 },
               new int[]{0,1,1,1,1,0,1,1,1,0 },
               new int[]{0,3,0,0,1,1,1,0,2,0 },
               new int[]{0,5,1,0,1,1,1,0,7,0 },
               new int[]{0,0,0,0,4,4,0,0,0,0 },
            };
			*/


			MapData tempMap = new MapData();       // 地图
												   // ArrayList mapList = new ArrayList();
			tempMap.map = tempMap.MapDataLoadTool(1);
            tempMap.id = tempRoom.SetRoomMap(tempMap);
            tempRoom.OpMap = tempMap;

            // System.Windows.Forms.Label GameMode;
            //*/
            //*/
            PictureBox[,] label1s = new PictureBox[10,10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0;j < 10; j++)
                {
                    // doDeBug("次数");
                    label1s[i, j] = new System.Windows.Forms.PictureBox();
                    if (label1s[i,j] != null)
                    {
                        label1s[i, j].Size = new System.Drawing.Size(600/10, 600/10);
                        int x = j * (600 / 10), y = i *(600 / 10);
                        // doDeBug("次数:"+x+",,"+y);
                        label1s[i, j].Location = new System.Drawing.Point(x,y);
                        label1s[i, j].SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;;
                        System.Drawing.Image img;
                        //*/
                        // 0：墙  1：路 2：钥匙 3：门 4：入口楼梯 5：宝箱 6 npc 7 敌人 8：出口楼梯
                        img = mapRes[tempRoom.Maps[tempMap.id].map[i][j]];
                            //*/
                        label1s[i, j].Image = img;
                        this.panel1.Controls.Add(label1s[i, j]);                 
                    }
                } 
            }
            tempMap.mapPic = label1s;
            return tempMap.id;
        }
        public void reLoad_PeoPle(int mapId)
        {
            // int id,string name,int camp,int Aggressivity,int Defenses,int LifeValue,string desc
            RoleData role = new RoleData(1, "老司机", 1, 1, 1, 10, "无所畏惧");
            role.Type = 1;
            role.UpOrDown1 = 1;
            role.LifeValue1 = 10;

            tempRoom.Role = role;

            this.heropic = new PictureBox();
            this.heropic.Size = new System.Drawing.Size(600 / 10, 600 / 10);
            this.heropic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heropic.Image = mapRes[MAPRES_LEAD];
            int x = 0, y = 0;
            foreach (int[] i in tempRoom.OpMap.map) {
                foreach (int j in i)
                {
                    if (role.UpOrDown1 == 1 && j == 4)  // 入口
                    {
                        this.heropic.Location = new System.Drawing.Point(x * (600/10), (y - 1) * (600/10));
                        break;
                    }
                    else if (role.UpOrDown1 == 2 && j == 8) // 出口
                    {
                        this.heropic.Location = new System.Drawing.Point(x * (600 / 10), (y + 1) * (600 / 10));
                        break;
                    }
                    x++;
                }
                y++;
                x = 0;
            }
            this.panel1.Controls.Add(this.heropic);
            this.heropic.BringToFront();

        }
        public void doDeBug(string msg, bool isShowAll = true)
        {
            string tmsg = "[" + DateTime.Now.ToString() + "]\n" + msg;
            if (isShowAll)
            {
                Debug.Text = tmsg + "\n" + Debug.Text;
            }
            Console.WriteLine(tmsg);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            int yidongnum = 10;
            int x = this.heropic.Location.X;
            int y = this.heropic.Location.Y;
            switch (e.KeyCode) {
                case Keys.Up:
                    y = y - yidongnum;
                   
                    break;
                case Keys.Down:
                    y = y + yidongnum;
                 
                    break;
                case Keys.Left:
                    x = x - yidongnum;
                  
                    break;
                case Keys.Right:
                    x = x + yidongnum;
                  
                    break;
                default:
                    break;
            }

            if (x < 0 || y < 0 || (x + 60 - 1) > 600 || (y + 60 - 1) > 600)
            {
                doDeBug("超出范围",false);
                return;
            }
            // 0：墙  1：路 2：钥匙 3：门 4：入口楼梯 5：宝箱 6 npc 7 敌人 8：出口楼梯  9
            int re = checkWalk(y, x);
            string msg = "";
            switch (re)
            {
                case 0:
                    msg += "撞墙";
					bc.DMGTestTools();
                    break;
                case 1:
                    msg += "行走";
                    this.heropic.Location = new System.Drawing.Point(x, y);
                    break;
                case 2:
                    msg += "获得钥匙 ";
                    PropClass pp = new PropClass(1, "钥匙", "大概能开一个门吧");
                    doDeBug(msg);
                    updataRoleInfo(1, pp);
                    updataPicInfo(MAPRES_ROAD);
                    /*
                    tempRoom.OpMap.map[tempRoom.OpMap.OpPicX][tempRoom.OpMap.OpPicY] = MAPRES_ROAD;
                    tempRoom.OpMap.mapPic[tempRoom.OpMap.OpPicX,tempRoom.OpMap.OpPicY].Image = mapRes[MAPRES_ROAD];  // 换图
                    */
                    break;
                case 3:
                    PropClass temp = tempRoom.Role.GetPropByType(1);
                    if ( temp != null)
                    {
                        msg += "使用钥匙";
                        doDeBug(msg);
                        updataRoleInfo(2, temp);
                        updataPicInfo(MAPRES_ROAD);
                        /*
                        tempRoom.OpMap.map[tempRoom.OpMap.OpPicX][tempRoom.OpMap.OpPicY] = MAPRES_ROAD;
                        tempRoom.OpMap.mapPic[tempRoom.OpMap.OpPicX, tempRoom.OpMap.OpPicY].Image = mapRes[MAPRES_ROAD];  // 换图
                        */
                    }
                    break;
                case 4:
                    break;
                case 5:
                   
                    PropClass sword = new PropClass(2, "锈迹斑斑的剑", "总比空手强");
                    sword.Id = tempRoom.Role.setProp(sword);
                    msg += "获得道具:" + sword.Name;
                    if (tempRoom.Role.Arms == null)
                    {
                        updataRoleInfo(3, sword);
                    }
                    updataPicInfo(MAPRES_NOGOLD);
                    doDeBug(msg);
                    break;
                case 6:

                    break;
                case 7:
                    BaseRole br = new BaseRole("一只小怪:",3,3,2,2,5);
                    msg = tempRoom.Role.Name +  " vs " + br.Name;
                    doDeBug(msg);
                    fightKo(tempRoom.Role, br);

                    if (tempRoom.Role.LifeValue1 > 0)
                    {
                        msg = "win";
                    }else
                    {
                        msg = "game over";
                    }
                    doDeBug(msg);
                    updataRoleInfo(4, null);
                    updataPicInfo(MAPRES_ROAD);
                    break;
                case 8:  
                    // 上楼

                    break;
                case 9:
                    break;

            }
            doDeBug("按键：" + e.KeyCode + "提示：" + msg, false);
        }

        public int fightKo(BaseRole A, BaseRole B)  // 递归战斗 直到死亡
        {
            if (B.LifeValue1 > 0 && A.LifeValue1 > 0)
            {
                double Num = doFight(A, B);
                B.LifeValue1 -= Math.Round(Num,2);
                // doDeBug("战斗进行中,b.LifeValue = " + B.LifeValue1);
                return fightKo(B,A);
                
            }
            else
            {
                return 0; 
            }

        }

        public double doFight(BaseRole A,BaseRole b)  // 战斗方法
         {
            return 0.5;
        }

        public void updataPicInfo(int PicType)
        {
            tempRoom.OpMap.map[tempRoom.OpMap.OpPicX][tempRoom.OpMap.OpPicY] = PicType;
            tempRoom.OpMap.mapPic[tempRoom.OpMap.OpPicX, tempRoom.OpMap.OpPicY].Image = mapRes[PicType];  // 换图
        }
        // 事件更新 1:捡钥匙    2:使用钥匙   3:更新武器  4:更新生命值
        public void updataRoleInfo(int EventType,PropClass pp)
        {
            if (EventType == 1)
            {
                pp.Id = tempRoom.Role.setProp(pp);
                tempRoom.Role.Keynum += 1;
                RoleKey.Text = "key:" + tempRoom.Role.Keynum;
            }
            else if (EventType == 2)
            {
                tempRoom.Role.getProp().Remove(pp);
                tempRoom.Role.Keynum -= 1;
                RoleKey.Text = "key:" + tempRoom.Role.Keynum;
            }
            else if (EventType == 3) //武器
            {
                tempRoom.Role.Arms = pp;
                RoleArms.Text = "arms:" + tempRoom.Role.Arms.Name;
                // 更新猪脚攻击力
                tempRoom.Role.Aggressivity1 += 1;
            }
            else if (EventType == 4)
            {
                RoleBlood.Text = "Blood: " + tempRoom.Role.LifeValue1;
            }
        }

        public int checkWalk(int y,int x) // 根据坐标检测地图信息
        {
            int[][] tempMap = tempRoom.OpMap.map;
            int zero = checkEq(y, x, 0);
            if (zero == 0)
            {
                return zero;
            }
            

            if (tempMap[(int)Math.Truncate(y / 60.0)][(int)Math.Truncate(x / 60.0)] != 1 ||
                tempRoom.OpMap.map[(int)Math.Truncate((y + 60 - 1) / 60.0)][(int)Math.Truncate((x + 60 - 1) / 60.0)] != 1 ||
                tempRoom.OpMap.map[(int)Math.Truncate(y / 60.0)][(int)Math.Truncate((x + 60 - 1) / 60.0)] != 1 ||
                tempRoom.OpMap.map[(int)Math.Truncate((y + 60 - 1) / 60.0)][(int)Math.Truncate(x / 60.0)] != 1 
                ){
                    for (int i = 2;i < 8;i++) 
                {
                    if (checkEq(y,x,i) == i)
                    {
                        return i;
                    }
                }
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public int checkEq(int y,int x, int i)
        {
            int[][] tempMap = tempRoom.OpMap.map;
            if (tempMap[(int)Math.Truncate(y / 60.0)][(int)Math.Truncate(x / 60.0)] == i)
            {
                tempRoom.OpMap.OpPicX = (int)Math.Truncate(y / 60.0);
                tempRoom.OpMap.OpPicY = (int)Math.Truncate(x / 60.0);
                return i;
            }
            else if(
                tempRoom.OpMap.map[(int)Math.Truncate((y + 60 - 1) / 60.0)][(int)Math.Truncate((x + 60 - 1) / 60.0)] == i )
            {
                tempRoom.OpMap.OpPicX = (int)Math.Truncate((y + 60 - 1) / 60.0);
                tempRoom.OpMap.OpPicY = (int)Math.Truncate((x + 60 - 1) / 60.0);
                return i;
            }
            else if(
                tempRoom.OpMap.map[(int)Math.Truncate(y / 60.0)][(int)Math.Truncate((x + 60 - 1) / 60.0)] == i)
            {
                tempRoom.OpMap.OpPicX = (int)Math.Truncate(y / 60.0);
                tempRoom.OpMap.OpPicY = (int)Math.Truncate((x + 60 - 1) / 60.0);
                return i;
            }
            else if (
                tempRoom.OpMap.map[(int)Math.Truncate((y + 60 - 1) / 60.0)][(int)Math.Truncate(x / 60.0)] == i)
            {
                tempRoom.OpMap.OpPicX = (int)Math.Truncate((y + 60 - 1) / 60.0);
                tempRoom.OpMap.OpPicY = (int)Math.Truncate(x / 60.0);
                return i;
            }else
            {
                return 10086;
            }
        }
    }

    public class RoleData:BaseRole
    {
        int id;
        string name;
        int type;
        int camp;  //阵营

        int Aggressivity;  // 攻，防，血
        int Defenses;
        int LifeValue;

        PropClass arms;  // 武器
        PropClass Armor; // 防具

        string desc; // 描述
        int UpOrDown;  // 1 up 2 down
        int keynum = 0;

        List<PropClass> Props = new List<PropClass>(10);


        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public int UpOrDown1
        {
            get
            {
                return UpOrDown;
            }

            set
            {
                UpOrDown = value;
            }
        }

        public int Keynum
        {
            get
            {
                return keynum;
            }

            set
            {
                keynum = value;
            }
        }

        public PropClass Arms
        {
            get
            {
                return Arms1;
            }

            set
            {
                Arms1 = value;
            }
        }

        public PropClass Armor1
        {
            get
            {
                return Armor2;
            }

            set
            {
                Armor2 = value;
            }
        }

        public PropClass Arms1
        {
            get
            {
                return arms;
            }

            set
            {
                arms = value;
            }
        }

        public PropClass Armor2
        {
            get
            {
                return Armor;
            }

            set
            {
                Armor = value;
            }
        }

        public int setProp(PropClass pp)
        {
            

            Props.Add(pp);
            return Props.Count - 1;
        }

        public List<PropClass> getProp()
        {
            return Props;
        }

        public PropClass GetPropByType(int type)  // 获取道具
        {
            
            foreach(PropClass pp in Props)
            {
                if (pp.Type == 1)
                {
                    return pp; 
                }
            }
            return null;
        }

        public RoleData(){

        }
        public RoleData(int id,string name,int camp,int Aggressivity,int Defenses,int LifeValue,string desc)
        {
            this.id = id;
            this.Name = name;
            this.camp = camp;
            this.Aggressivity = Aggressivity;
            this.Defenses = Defenses;
            this.LifeValue = LifeValue;
            this.desc = desc;
        }
        
    }

    public class RoomClass
    {
        int id;
        int type;
        List <MapData> maps = new List<MapData>();
        RoleData role;
        MapData opMap; // 当前地图

        public RoleData Role
        {
            get
            {
                return role;
            }

            set
            {
                role = value;
            }
        }

        public List<MapData> Maps
        {
            get
            {
                return maps;
            }
        }

        public MapData OpMap
        {
            get
            {
                return opMap;
            }

            set
            {
                opMap = value;
            }
        }

        public int SetRoomMap(MapData map)
        {
            this.Maps.Add(map);
            return Maps.Count() - 1;
        }
 
    }
}
namespace GameBaseClass
{
    public class EnumProgram
    {
        enum gameInfo
        {
            wall = 0,
            road = 1,
            key = 2,
            door = 3,
            Entrance = 4,
            chest = 5,
            npc = 6,
            Enemy = 7,
            Exit = 8,
        };

        enum Camps
        {
            GoodNess = 1, //善良
            neutral = 2, // 中立
            evil = 3,   // 邪恶
        }

        enum RoleType
        {
            Lead = 1,
            npc = 2,
            Enemy = 3,
        }
    }
}
