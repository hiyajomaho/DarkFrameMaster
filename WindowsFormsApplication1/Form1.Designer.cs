﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.heropic = new System.Windows.Forms.PictureBox();
            this.Debug = new System.Windows.Forms.Label();
            this.Rolename = new System.Windows.Forms.Label();
            this.RoleArms = new System.Windows.Forms.Label();
            this.RoleKey = new System.Windows.Forms.Label();
            this.RoleArmor = new System.Windows.Forms.Label();
            this.RoleAttack = new System.Windows.Forms.Label();
            this.RoleBlood = new System.Windows.Forms.Label();
            this.RoleDefense = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heropic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.heropic);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 600);
            this.panel1.TabIndex = 0;
            // 
            // heropic
            // 
            this.heropic.Location = new System.Drawing.Point(0, 0);
            this.heropic.Name = "heropic";
            this.heropic.Size = new System.Drawing.Size(0, 0);
            this.heropic.TabIndex = 0;
            this.heropic.TabStop = false;
            // 
            // Debug
            // 
            this.Debug.AutoSize = true;
            this.Debug.Location = new System.Drawing.Point(605, 9);
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(41, 12);
            this.Debug.TabIndex = 1;
            this.Debug.Text = "label1";
            // 
            // Rolename
            // 
            this.Rolename.AutoSize = true;
            this.Rolename.Location = new System.Drawing.Point(741, 9);
            this.Rolename.Name = "Rolename";
            this.Rolename.Size = new System.Drawing.Size(71, 12);
            this.Rolename.TabIndex = 2;
            this.Rolename.Text = "name:喵喵喵";
            // 
            // RoleArms
            // 
            this.RoleArms.AutoSize = true;
            this.RoleArms.Location = new System.Drawing.Point(741, 161);
            this.RoleArms.Name = "RoleArms";
            this.RoleArms.Size = new System.Drawing.Size(47, 12);
            this.RoleArms.TabIndex = 3;
            this.RoleArms.Text = "arms:无";
            // 
            // RoleKey
            // 
            this.RoleKey.AutoSize = true;
            this.RoleKey.Location = new System.Drawing.Point(741, 130);
            this.RoleKey.Name = "RoleKey";
            this.RoleKey.Size = new System.Drawing.Size(35, 12);
            this.RoleKey.TabIndex = 4;
            this.RoleKey.Text = "key:0";
            // 
            // RoleArmor
            // 
            this.RoleArmor.AutoSize = true;
            this.RoleArmor.Location = new System.Drawing.Point(741, 196);
            this.RoleArmor.Name = "RoleArmor";
            this.RoleArmor.Size = new System.Drawing.Size(53, 12);
            this.RoleArmor.TabIndex = 5;
            this.RoleArmor.Text = "Armor:无";
            // 
            // RoleAttack
            // 
            this.RoleAttack.AutoSize = true;
            this.RoleAttack.Location = new System.Drawing.Point(741, 70);
            this.RoleAttack.Name = "RoleAttack";
            this.RoleAttack.Size = new System.Drawing.Size(53, 12);
            this.RoleAttack.TabIndex = 6;
            this.RoleAttack.Text = "attack:1";
            // 
            // RoleBlood
            // 
            this.RoleBlood.AutoSize = true;
            this.RoleBlood.Location = new System.Drawing.Point(741, 41);
            this.RoleBlood.Name = "RoleBlood";
            this.RoleBlood.Size = new System.Drawing.Size(53, 12);
            this.RoleBlood.TabIndex = 7;
            this.RoleBlood.Text = "Blood:10";
            // 
            // RoleDefense
            // 
            this.RoleDefense.AutoSize = true;
            this.RoleDefense.Location = new System.Drawing.Point(741, 100);
            this.RoleDefense.Name = "RoleDefense";
            this.RoleDefense.Size = new System.Drawing.Size(59, 12);
            this.RoleDefense.TabIndex = 8;
            this.RoleDefense.Text = "defense:1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 600);
            this.Controls.Add(this.RoleDefense);
            this.Controls.Add(this.RoleBlood);
            this.Controls.Add(this.RoleAttack);
            this.Controls.Add(this.RoleArmor);
            this.Controls.Add(this.RoleKey);
            this.Controls.Add(this.RoleArms);
            this.Controls.Add(this.Rolename);
            this.Controls.Add(this.Debug);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(300, 0);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.heropic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Debug;
        private System.Windows.Forms.PictureBox heropic;
        private System.Windows.Forms.Label Rolename;
        private System.Windows.Forms.Label RoleArms;
        private System.Windows.Forms.Label RoleKey;
        private System.Windows.Forms.Label RoleArmor;
        private System.Windows.Forms.Label RoleAttack;
        private System.Windows.Forms.Label RoleBlood;
        private System.Windows.Forms.Label RoleDefense;
    }
}

