﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace operationClass
{
    public class PropClass
    {
        int id;
        int type;
        string name;
        string desc;

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return Name1;
            }

            set
            {
                Name1 = value;
            }
        }

        public string Desc
        {
            get
            {
                return desc;
            }

            set
            {
                desc = value;
            }
        }

        public string Name1
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public PropClass(int type,string name,string desc)
        {
            this.Type = type;
            this.Name = name;
            this.Desc = desc;

        }
    }
}
